// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Cpp_MazeGeneratorTarget : TargetRules
{
	public Cpp_MazeGeneratorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Cpp_MazeGenerator" } );
	}
}
