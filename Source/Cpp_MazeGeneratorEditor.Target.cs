// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Cpp_MazeGeneratorEditorTarget : TargetRules
{
	public Cpp_MazeGeneratorEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Cpp_MazeGenerator" } );
	}
}
