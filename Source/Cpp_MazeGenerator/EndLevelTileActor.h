// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Components/BoxComponent.h"
#include "EndLevelTileActor.generated.h"

/**
 *  EndLevelTileActor to represent the End of Level Tile and handling the spawning of the next level if available
 */
UCLASS()
class CPP_MAZEGENERATOR_API AEndLevelTileActor : public AStaticMeshActor
{
	GENERATED_BODY()
	
	/* Called when something enters the box component */
	UFUNCTION()		
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	/* Called when something leaves the box component */
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void Debug(FString msg);
public:
	AEndLevelTileActor();
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CollidingComponent)
		UBoxComponent* CollisionBox;
};
