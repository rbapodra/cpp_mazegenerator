// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleBoxTrigger.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "MazeHUD.h"
#include "AMazeGameMode.h"

// Sets default values
ASimpleBoxTrigger::ASimpleBoxTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxTrigger"));
	Box->bGenerateOverlapEvents = true;
	Box->SetRelativeScale3D(FVector(6.5, 6.25, 1));  //got this number after testing on the ground tile BP (this could be adjusted from the editor)
	RootComponent = Box;

	//Bind the Overlap events
	Box->OnComponentBeginOverlap.AddDynamic(this, &ASimpleBoxTrigger::TriggerEnter);
	Box->OnComponentEndOverlap.AddDynamic(this, &ASimpleBoxTrigger::TriggerExit);
}

void ASimpleBoxTrigger::TriggerEnter(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	FString ActorName = OtherActor->GetActorLabel();
	if (ActorName == "BP_ThirdPersonCharacter")
	{
		Debug(FString("Game Over! Level Completed!"));

		//TODO:: need to move this code in an object specific trigger
		//Change to the next level
		//NOTE:: we will have to keep track of the current level we are in, otherwise there is a possibility this might go in an infinite loop
		//Ideally, do this in Level Blueprint
		//For now we will keep track of it in the GameMode class
		auto world = GetWorld();
		FString CurrentLevelName = UGameplayStatics::GetCurrentLevelName(world);
		Debug("Current LevelName : " + CurrentLevelName);
		
		AAMazeGameMode* pMyGameMode = Cast<AAMazeGameMode>(GetWorld()->GetAuthGameMode());
		if (!pMyGameMode)
		{
			Debug(FString("Invalid GameMode!"));
			return;
		}
		FName newLevelName = pMyGameMode->GetNextLevelName(FName(*CurrentLevelName));
		if (CurrentLevelName != newLevelName.ToString() && newLevelName != "None")
		{
			UGameplayStatics::OpenLevel(world, newLevelName);
		}
		else
		{
			Debug(FString("GAME OVER!! All Levels Completed!"));
			//Get the HUD and turn on the widget
			AMazeHUD* pMazeHUD = Cast<AMazeHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
			if (pMazeHUD)
			{
				pMazeHUD->ShowGameOverWidget();
			}
		}
		
	}
	else
	{
		Debug(FString("Trigger Enter!"));
	}
	
}

void ASimpleBoxTrigger::TriggerExit(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Debug(FString("Trigger Exit!"));
}

/*
* Display a text message for Debugging purposes
*/
void ASimpleBoxTrigger::Debug(FString msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, msg);
	}
}
