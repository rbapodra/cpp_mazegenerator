// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MazePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CPP_MAZEGENERATOR_API AMazePlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AMazePlayerController(const FObjectInitializer& ObjectInitializer);
	virtual void PostInitializeComponents() override;

	virtual void ReceivedPlayer() override;
};
