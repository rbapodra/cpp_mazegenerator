// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AMazeGameMode.generated.h"

/**
 * 
 */
UCLASS()
class CPP_MAZEGENERATOR_API AAMazeGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AAMazeGameMode();	

	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

	void ResetPlayerSpectatorMode();

	FName GetNextLevelName(const FName& FCurrentLevelName);
private:
	UClass* FindClasses(const FString& ClassName) const;
	
	TArray<FName> FArrayLevelNames;
};
