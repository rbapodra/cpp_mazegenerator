// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimelineComponent.h"
#include "TimelineDoorActor.generated.h"

/*
**\TimelineDoorActor is the same as DoorActor except the door rotation is carried out using a timeline rather than through LERP in the tick function
*/

UCLASS()
class CPP_MAZEGENERATOR_API ATimelineDoorActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATimelineDoorActor();

	/* Returns whether or not the door is Open*/
	UFUNCTION(BlueprintPure, Category = "DoorState")
		bool IsOpen();

	/* Allows other classes to safely change the state of the Door*/
	UFUNCTION(BlueprintCallable, Category = "DoorState")
		void SetState(bool bNewState);

	/*Every door has a particular key with which it can be opened */
	UPROPERTY(EditInstanceOnly)
		FString OpensWithKeyName;

	/* DoorInfo blueprint class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widgets")
		TSubclassOf<class UUserWidget> wDoorInfoClass; //We will be using the BP_DoorInfoWidget asset

	 /* Curve representing the rotation of the door */
	UPROPERTY(EditAnywhere)
		class UCurveFloat* DoorRotateCurve;

	/* Opens the Door. This will be called on button press */
	UFUNCTION(BlueprintCallable)
		void OpenDoor();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* State of the door: Open or Closed. True means Open */
	bool bDoorState;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/* Mesh representing the DoorWall */
	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* DoorWallMesh;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* DoorMesh;

	/*Box Component to be used as a collider */
	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* CollisionBox;

	/* The UI showing Press E to open when the player gets close */
	class UUserWidget* DoorInfoWidget;

	/* Called when something enters the box component */
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/* Called when something leaves the box component */
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void Debug(FString msg);

	/* Rotation related attributes and functions */
	float RotateValue;
	float TimelineValue;
	float DoorCurveValue; //at a particular point in time
	FRotator DoorRotation;
	FTimeline DoorTimeline;

	bool bReadyState;

	UFUNCTION()
		void ControlDoor();

	UFUNCTION()
		void ToggleDoor(); //called by Player

	UFUNCTION()
		void CloseDoor(); //automatically close the door if the player is not in the vicinity of the Door
};
