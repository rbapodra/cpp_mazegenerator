// Fill out your copyright notice in the Description page of Project Settings.

#include "TimelineDoorActor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "AThirdPersonCharacter.h"
#include "Engine/Engine.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "Curves/CurveFloat.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
ATimelineDoorActor::ATimelineDoorActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxTrigger"));
	CollisionBox->bGenerateOverlapEvents = true;
	CollisionBox->InitBoxExtent(FVector(50.0f, 50.0f, 50.0f));

	RootComponent = CollisionBox; //make the collision box as the root component

	DoorWallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorWall"));
	//attach this to the root component
	DoorWallMesh->SetupAttachment(RootComponent);

	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorFrame"));
	DoorMesh->SetupAttachment(RootComponent); //TODO:: need to check this one


	//Bind the Overlap events
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &ATimelineDoorActor::OnOverlapBegin);
	CollisionBox->OnComponentEndOverlap.AddDynamic(this, &ATimelineDoorActor::OnOverlapEnd);

	bDoorState = false; //initially door is closed
	bReadyState = true; //ready to open
	OpensWithKeyName = ""; //Set this from the editor

	//initialize the Door Widget
	static ConstructorHelpers::FClassFinder<UUserWidget> widgetObject(TEXT("/Game/Blueprints/BP_DoorInfoWidget"));
	if (widgetObject.Succeeded())
	{
		wDoorInfoClass = widgetObject.Class;
	}
}

void ATimelineDoorActor::OpenDoor()
{
	Debug("Open Door");
	//SetState(true);
	//Hide the UI as well
	DoorInfoWidget->SetVisibility(ESlateVisibility::Hidden);
	/*if (IsOpen())
	{
		RotateValue = 1.0f;
		DoorTimeline.PlayFromStart();
	}
	*/
	ToggleDoor();
}

// Called when the game starts or when spawned
void ATimelineDoorActor::BeginPlay()
{
	Super::BeginPlay();

	RotateValue = 1.0f;

	if (DoorRotateCurve) //if there is a valid Rotate Curve, setup the timeline
	{
		//set-up callback for the timeline
		FOnTimelineFloat TimelineCallback;
		FOnTimelineEventStatic TimelineFinishedCallback;

		//Bind the functions
		TimelineCallback.BindUFunction(this, FName("ControlDoor"));
		TimelineFinishedCallback.BindUFunction(this, FName("SetState"));

		DoorTimeline.AddInterpFloat(DoorRotateCurve, TimelineCallback);		
		DoorTimeline.SetTimelineFinishedFunc(TimelineFinishedCallback);				
	}
		
	//create the widget object
	if (wDoorInfoClass)
	{
		//create the widget and display it
		DoorInfoWidget = CreateWidget<UUserWidget>(GetWorld(), wDoorInfoClass);
		if (DoorInfoWidget)
		{
			DoorInfoWidget->AddToViewport();
			DoorInfoWidget->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

// Called every frame
void ATimelineDoorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DoorTimeline.TickTimeline(DeltaTime);
}

bool ATimelineDoorActor::IsOpen()
{
	return bDoorState;
}

void ATimelineDoorActor::SetState(bool bNewState)
{
	bDoorState = bNewState;
	
	bReadyState = true;
}

void ATimelineDoorActor::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		FString ActorName = OtherActor->GetActorLabel();
		if (ActorName == "BP_ThirdPersonCharacter")
		{
			//Display the UI ONLY if the Door is closed 
			if (!IsOpen())
			{
				DoorInfoWidget->SetVisibility(ESlateVisibility::Visible);
			}
			
		}
	}
}

void ATimelineDoorActor::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		FString ActorName = OtherActor->GetActorLabel();
		if (ActorName == "BP_ThirdPersonCharacter")
		{
			//Display the UI
			DoorInfoWidget->SetVisibility(ESlateVisibility::Hidden);
			//if the door is open, close it
			if (IsOpen())
			{
				CloseDoor(); 
			}
		}
	}
}

void ATimelineDoorActor::Debug(FString msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, msg);
	}
}

void ATimelineDoorActor::ControlDoor()
{
	TimelineValue = DoorTimeline.GetPlaybackPosition();
	DoorCurveValue =  RotateValue * DoorRotateCurve->GetFloatValue(TimelineValue);

	//new rotation value be an interpolated value got from the curve
	FQuat NewRotation = FQuat(FRotator(0.0f, DoorCurveValue, 0.0f));
	//Set this rotation
	DoorMesh->SetRelativeRotation(NewRotation);

}

void ATimelineDoorActor::ToggleDoor()
{
	if (bReadyState)
	{
		bDoorState = !bDoorState;
		
		//Get the direction the player is facing and open in that direction
		APawn* OurPawn = UGameplayStatics::GetPlayerPawn(this, 0);
		FVector PawnLocation = OurPawn->GetActorLocation();
		FVector Direction = PawnLocation - GetActorLocation();
		Direction = UKismetMathLibrary::LessLess_VectorRotator(Direction, GetActorRotation());
		
		DoorRotation = DoorMesh->RelativeRotation;

		if (Direction.X > 0.0f)
		{
			RotateValue = 1.0f;
		}
		else
		{
			RotateValue = -1.0f;
		}
		

		bReadyState = false;
		DoorTimeline.PlayFromStart();
	}
	else
	{
		bReadyState = false;
		DoorTimeline.Reverse();
	}
}

void ATimelineDoorActor::CloseDoor()
{
	if (bReadyState)
	{
		bReadyState = false;
		DoorTimeline.Reverse();
	}
}