// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupActor.generated.h"

/*
**\ This class represents an Actor object that can be picked. For Eg: A Key or a health pack
*/

UCLASS()
class CPP_MAZEGENERATOR_API APickupActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupActor();

	//Get accessor for the mesh 
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return PickupMesh; }

	/* Returns whether or not the pickup is Active*/
	UFUNCTION(BlueprintPure, Category = "Pickup")
	bool IsActive();

	/* Allows other classes to safely change if the Pickup is active*/
	UFUNCTION(BlueprintCallable, Category = "Pickup")
	void SetActive(bool bNewState);

	/* Called when something enters the box component */
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
		void Debug(FString msg);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* True if the object can be picked*/
	bool bIsActive; 
private:
	//Static Mesh component to represent the mesh of the object 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickup", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* PickupMesh;  //this is a sort of forward declaration (the class notation)
	
	//Box Component to be used as a collider
	UPROPERTY(VisibleAnywhere)
	class UBoxComponent* CollisionBox;
};
