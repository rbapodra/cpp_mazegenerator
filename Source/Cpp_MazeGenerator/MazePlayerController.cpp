// Fill out your copyright notice in the Description page of Project Settings.

#include "MazePlayerController.h"
#include "GameFrameWork/PlayerState.h"

AMazePlayerController::AMazePlayerController(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
}

void AMazePlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AMazePlayerController::ReceivedPlayer()
{
	//initially we need the player to be a spectator until the maze generation is complete. Once done, we spawn the player at the appropriate location.
//	PlayerState->bIsSpectator = true; //NOTE:: uncomment only for a full procedural maze
	Super::ReceivedPlayer();
}