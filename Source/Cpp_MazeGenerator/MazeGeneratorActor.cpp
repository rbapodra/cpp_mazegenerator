// Fill out your copyright notice in the Description page of Project Settings.

#include "MazeGeneratorActor.h"
#include "AMazeGameMode.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"
#include "SimpleBoxTrigger.h"


//class static variable init
const float AMazeGeneratorActor::offset = 400.0f;
const float AMazeGeneratorActor::BlockPadding = 200.0f;
const FRotator AMazeGeneratorActor::rotation = FRotator(0.0f, 0.0f, 0.0f);

// Sets default values
AMazeGeneratorActor::AMazeGeneratorActor()	
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Rows = MazeSizeMax;
	Columns = MazeSizeMax;
	ShowTests = false;
	StartX = -2000;
	StartY = -2000;
	StartZ = -200;

	//Initialize the maze grid; this will tell whether a particular cell is visited or not and in which direction
	for (int i = 0; i < Rows; ++i)
	{
		MazeGrid.Add(FIntGrid());
	}
	for (int i = 0; i < Rows; ++i)
	{
		for (int j = 0; j < Columns; ++j)
		{
			MazeGrid[i].Columns.Add(0);
		}
	}

	for (int i = 0; i < MazeSizeMax; i++)
	{
		for (int j = 0; j < MazeSizeMax; j++)
		{
			grid[i][j] = nullptr;
		}
	}
}

// Called when the game starts or when spawned
void AMazeGeneratorActor::BeginPlay()
{
	Super::BeginPlay();
	
	//if (ShowTests)
	//{
		//SpawnMaze();
		
		//ListAllCurrentActors();

		//Not the best method but this is just for test
		//Note:: this is only for full procedural code
		/*AAMazeGameMode* pMyGameMode = dynamic_cast<AAMazeGameMode*>(GetWorld()->GetAuthGameMode());
		if (pMyGameMode)
		{
			//APlayerController* myPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			APlayerController* myPlayerController = GetWorld()->GetFirstPlayerController();
			if (myPlayerController)
			{		
				//Don't need to do a changeState because RestartPlayer will already do it
				//myPlayerController->ChangeState(NAME_Playing);
				FVector SpawnLoc = grid[1][1]->GetActorLocation();
				//need to adjust this spawn location upward and forward
				SpawnLoc.X  += 200;
				SpawnLoc.Y += 300;
				SpawnLoc.Z += 100;
				const FVector  GenSpawnLoc(SpawnLoc);
				const FTransform GenSpawnTransfrom(GenSpawnLoc);
				
				pMyGameMode->RestartPlayerAtTransform(myPlayerController, GenSpawnTransfrom);
								
			}			
		}
		*/
	//}
}

// Called every frame
void AMazeGeneratorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/*!
**!Maze Generation root function
*/
void AMazeGeneratorActor::GenerateMaze()
{
	auto World = GetWorld();
	if (!World || !TileBlockBP || !TileGroundBP || !TileStartBP || !TileEndBP)
	{
		return;
	}

	//OddAlgo();	
	//DFS();
	Kruskal();
}

/*!
**\OddAlgo to generate the maze 
**\This algorithm needs an odd maze dimension and initially puts walls on every other location
*/
void AMazeGeneratorActor::OddAlgo()
{
	auto World = GetWorld();
	float CaptureX = 0.0f;
	float CaptureY = 0.0f;

	//if we have even dimension, change it to ODD
	if (Rows % 2 == 0)
	{
		Rows++;
	}
	if (Columns % 2 == 0)
	{
		Columns++;
	}

	/*!
	**\Initially generate the maze to look like this: 
	*			###########
	*			#---------#
	*			#-#-#-#-#-#
	*			#---------#
	*			#-#-#-#-#-#
	*			#---------#
	*			#-#-#-#-#-#
	*			#---------#
	*			###########
	*/

	//Build a Squares X
	for (int row = 0; row < Rows; row++)
	{
		//Build a Squares Y
		for (int col = 0; col < Columns; col++)
		{
			//Spawn blocks at the border or for all even values
			if (col == 0 || row == 0 || col == Columns - 1 || row == Rows - 1 || col % 2 == 0 && row % 2 == 0)
			{
				//                          (X.Xf,Y.Yf,Z.Zf)
				const FVector  GenSpawnLoc(CaptureX, CaptureY, 0.0f);
				AStaticMeshActor* BlockTile = SpawnTileBP<AStaticMeshActor>(World, TileBlockBP, GenSpawnLoc, rotation);

				grid[row][col] = BlockTile;
			}
			else
			{
				//for all other values, spawn a floor tile
				const FVector  GenSpawnLoc(CaptureX, CaptureY, 0.0f);
				AStaticMeshActor* GroundTile = SpawnTileBP<AStaticMeshActor>(World, TileGroundBP, GenSpawnLoc, rotation);

				grid[row][col] = GroundTile;
			}

			//-------------Starting Tile Spawn---------------
			if (CaptureX == offset && CaptureY == offset) //starting tile will be spawned at the diagonal location 1,1
			{
				//First destroy the existing tile and then create a new starting tile object
				grid[1][1]->Destroy();

				const FVector  GenSpawnLoc(offset, offset, 0.0f);
				//Tile Start
				AStaticMeshActor* StartTile = SpawnTileBP<AStaticMeshActor>(World, TileStartBP, GenSpawnLoc, rotation);

				grid[1][1] = StartTile;
			}

			//-------------Ending Tile Spawn---------------
			if (col == Columns - 1 && row == Rows - 1) //ending tile will be spawned at the last location on the grid
			{
				grid[row - 1][col - 1]->Destroy();

				const FVector  GenSpawnLoc(((Rows - 2) * offset), ((Columns - 2) * offset), 0);
				// Tile End
				AStaticMeshActor* EndTile = SpawnTileBP<AStaticMeshActor>(World, TileEndBP, GenSpawnLoc, rotation);

				grid[row - 1][col - 1] = EndTile;
			}

			CaptureY += offset;
			if (CaptureY >= offset * Columns)
			{
				CaptureY = 0;
			}
		}

		CaptureX += offset;
		if (CaptureX >= offset * Rows)
		{
			CaptureX = 0;
		}
	}
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//Now we know all even locations other than the perimeter have walls (Block tile), so we iterate through each wall other than the outside
	//and randomly chose a direction, if the new position is not occupied by a wall, then add a wall to it
	//this particular iteration will start from (2,2) and iterate over all col and this configuration is for a 4 way corridor (since all the odd places (+1 increment) will be a floor in the beginning)
	for (int col = 2; col < Columns - 1; col += 2)
	{
		int drow = 2;
		int dcol = col;
		int rnd4 = rand() % 4;

		switch (rnd4)
		{
		case 0: drow++; break; //East
		case 1: drow--; break; //West
		case 2: dcol++; break; //North
		case 3: dcol--; break; //South
		}

		if (grid[drow][dcol]->GetActorLabel() != "Block")
		{
			FVector f = grid[drow][dcol]->GetActorLocation();
			grid[drow][dcol]->Destroy();

			const FVector  GenSpawnLoc(f);

			AStaticMeshActor* BlockTile = SpawnTileBP<AStaticMeshActor>(World, TileBlockBP, GenSpawnLoc, rotation);
			grid[drow][dcol] = BlockTile;
		}
		else
		{
			col -= 2; //if this is a block, it means this cell was visited and so we need to back-track
		}
	}

	//in the previous iteration, we iterated over col = 2 and above with x taking a max possible value of 3: every iteration will be (3,2), (3,4)..
	//now iterate over all the middle even cells which will usually be a 3 sided.
	for (int row = 4; row < Rows - 1; row += 2)
	{
		for (int col = 2; col < Columns - 1; col += 2)
		{
			int drow = row;
			int dcol = col;
			int rnd3 = rand() % 3;

			switch (rnd3)
			{
			case 0: dcol++; break; //North
			case 1: dcol--; break; //South
			case 2: drow++; break; //East
			}

			if (grid[drow][dcol]->GetName() != "Block")
			{
				FVector f = grid[drow][dcol]->GetActorLocation();
				grid[drow][dcol]->Destroy();

				const FVector  GenSpawnLoc(f);

				AStaticMeshActor* BlockTile = SpawnTileBP<AStaticMeshActor>(World, TileBlockBP, GenSpawnLoc, rotation);
				grid[drow][dcol] = BlockTile;
			}
			else
			{
				row -= 2; //back-track
			}
		}
	}
}

/*!
**\DFS algorithm to generate the maze. This is essentially the same as the RecursiveBackTracking algo
*/
void AMazeGeneratorActor::DFS()
{
	auto World = GetWorld();

	//first generate a wall for all the cells. then we will call recursive DFS algo to carve a path through all the walls.
	for (int row = 0; row < Rows; row++)
	{
		for (int col = 0; col < Columns; col++)
		{
			const FVector  GenSpawnLoc(StartX + col * offset, StartY + row * offset, StartZ);			
			AStaticMeshActor* BlockTile = SpawnTileBP<AStaticMeshActor>(World, TileBlockBP, GenSpawnLoc, rotation);
			grid[row][col] = BlockTile;
			boolWallGrid[row][col] = true;			
		}
	}

	//Now recursively call the DFS algo to carve a path through the walls
	DFSRecursive(1, 1);

	//Spawn the start and end tiles
	//these tiles will not need padding since these would already been added when using the ground tile
	{
		FVector GenSpawnLoc = grid[1][1]->GetActorLocation();
		grid[1][1]->Destroy();
		//const FVector  GenSpawnLoc( StartX + 2 * offset, StartY + 2 * offset, StartZ);
		//Tile Start
		AStaticMeshActor* StartTile = SpawnTileBP<AStaticMeshActor>(World, TileStartBP, GenSpawnLoc, rotation);
		grid[1][1] = StartTile;
		boolWallGrid[1][1] = false;
	}

	{
		FVector GenSpawnLoc = grid[Rows - 2][Columns - 2]->GetActorLocation();
		grid[Rows - 2][Columns - 2]->Destroy();
		//FVector  GenSpawnLoc(StartX + ((Columns - 2) * offset), (StartY + (Rows - 2) * offset), StartZ);
		// Tile End
		AStaticMeshActor* EndTile = SpawnTileBP<AStaticMeshActor>(World, TileEndBP, GenSpawnLoc, rotation);
		grid[Rows - 2][Columns - 2] = EndTile;
		boolWallGrid[Rows - 2][Columns - 2] = false;
	}
	
}

/*!
**\This function only encapsulates the entire maze generation process
**\Note:: the event that calls this function can only generate the maze once after this actor is created (whether in runtime or in the editor)
*/
void AMazeGeneratorActor::SpawnMaze()
{
	//generate the maze ONLY if the maze is not created, do not keep generating again and again
	if (grid[0][0] == nullptr)
	{
		GenerateMaze();
		SpawnEndLevelTrigger();
	}
	
}

/*!
**\The main logic of the DFS algorithm is handled by this function in a recursive fashion
*/
void AMazeGeneratorActor::DFSRecursive(int row, int col)
{
	auto World = GetWorld();

	//first destroy the current wall and spawn a ground tile at the current location row, col
	grid[row][col]->Destroy();
	
	FVector  GenSpawnLoc(StartX + col * offset, StartY + row * offset, StartZ);
	GenSpawnLoc.Y -= BlockPadding;

	AStaticMeshActor* GroundTile = SpawnTileBP<AStaticMeshActor>(World, TileGroundBP, GenSpawnLoc, rotation);
	grid[row][col] = GroundTile;
	boolWallGrid[row][col] = false;

	//now we need to choose 1 of the 4 adjacent tiles at random that are occupied by a BLOCK tile
	
	//keep a dx array that stores the x increment and a dy array that keeps the y increment (+1 or -1)
	int dx[4] = { 1,-1,0,0 };
	int dy[4] = { 0, 0, -1, 1 };
	//keep an array to store the above array indices (0-3)
	TArray<int8> indices;
	indices.Add(0);
	indices.Add(1);
	indices.Add(2);
	indices.Add(3);
	//now sort these indices (we are using a lambda function as a predicate for the comparison)
	indices.Sort([this](const int8 item1, const int8 Item2) {
		return FMath::FRand() < 0.5f;
	});

	int32 NewRow, NewCol;
	bool flag = true; //using a flag to determine valid location
	//now traverse through the randomized array indices and pick a valid cell location and apply this function recursively
	for (int i = 0; i < 4; i++)
	{
		NewRow = row + dy[indices[i]];
		NewCol = col + dx[indices[i]];

		//check if the (NewRow, NewCol) is a valid location
		if (NewRow >= 1 && NewRow <= Rows - 1 && NewCol >= 1 && NewCol <= Columns - 1 && boolWallGrid[NewRow][NewCol])
		{
			flag = true;
			//check if any of the previous path is the original source or a wall to possibly avoid loops
			for (int j = 0; j < 4; ++j)
			{
				flag = flag && (boolWallGrid[NewRow + dy[j]][NewCol + dx[j]] || (NewRow + dy[j] == row && NewCol + dx[j] == col));
			}
			if (flag && NewRow != Rows - 1 && NewCol != Columns - 1) //avoiding the final column
			{
				DFSRecursive(NewRow, NewCol);
			}
		}
	}
}

/*!
**\Who should control the spawning of this trigger?
*/
void AMazeGeneratorActor::SpawnEndLevelTrigger()
{
	//Find the EndTile actor and grab its location. Spawn the trigger object in the middle of this tile.
	//We can use the hard coded mazeGrid which could give us the answer right away but if we decide to choose a random ending point, we are done for
	FVector SpawnLoc;
	bool bEndLevelTileFound = false;
	for (TActorIterator<AStaticMeshActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (ActorItr->GetActorLabel() == FString("EndTile"))
		{
			SpawnLoc = ActorItr->GetActorLocation();
			bEndLevelTileFound = true;
			break;
		}
	}

	if (bEndLevelTileFound)
	{
		//Note:: found this adjustments after testing roughly
		SpawnLoc.X += 160;
		SpawnLoc.Y += 100;
		SpawnLoc.Z += 10;

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = (ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		ASimpleBoxTrigger* BoxTrigger = GetWorld()->SpawnActor<ASimpleBoxTrigger>(SpawnLoc, rotation, SpawnInfo);
		BoxTrigger->SetActorLabel(FString("LevelEndTrigger"));
	}
	
}

void AMazeGeneratorActor::ListAllCurrentActors()
{
	for (TActorIterator<AStaticMeshActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		FString fsActorName = ActorItr->GetName();
		FName fActorClassName = ActorItr->GetClass()->GetFName();
	}		
}

FCoordinate AMazeGeneratorActor::Find(const FCoordinate& node)
{
	if (Parent[node.row][node.col] == node)
	{
		return node;
	}

	return Parent[node.row][node.col] =  Find(Parent[node.row][node.col]);
}

void AMazeGeneratorActor::Union(const FCoordinate& node1, const FCoordinate& node2)
{
	//this is going to be a weighted Union based on the rank
	if (rank[node1.row][node1.col] == rank[node2.row][node2.col])
	{
		Parent[node1.row][node1.col] = node2;
		rank[node2.row][node2.col]++;
	}
	else if (rank[node1.row][node1.col] < rank[node2.row][node2.col])
	{
		Parent[node1.row][node1.col] = node2;		
	}
	else
	{
		Parent[node2.row][node2.col] = node1;
	}
}

/*!
**\Kruskal's Algorithm to generate the maze
*/
void AMazeGeneratorActor::Kruskal()
{
	auto World = GetWorld();

	//first we need to construct the graph with edges
	//every floor tile initially will be its own subset (or parent)
	//to do this we follow the same approach we used in OddEven Algo by generating the wall at all even locations
	//but this time we will need to update the Parent and the Rank arrays

	//Build a Squares X
	for (int row = 0; row < Rows; row++)
	{
		//Build a Squares Y
		for (int col = 0; col < Columns; col++)
		{
			//Spawn blocks at the border or for all even values
			if (row != 0 && col != 0 && row != Rows - 1 && col != Columns - 1 && row % 2 == 1 && col % 2 == 1)
			{
				//for all other values, spawn a floor tile
				FVector  GenSpawnLoc(StartX + col * offset, StartY + row * offset, StartZ);
				GenSpawnLoc.Y -= BlockPadding;

				AStaticMeshActor* GroundTile = SpawnTileBP<AStaticMeshActor>(World, TileGroundBP, GenSpawnLoc, rotation);

				grid[row][col] = GroundTile;
				boolWallGrid[row][col] = false;
				rank[row][col] = 0; //initialize the rank value with 0
				Parent[row][col] = FCoordinate(row, col); //this cell will be its own parent(or root) initially
			}
			else
			{
				//                          (X.Xf,Y.Yf,Z.Zf)
				const FVector  GenSpawnLoc(StartX + col * offset, StartY + row * offset, StartZ);
				AStaticMeshActor* BlockTile = SpawnTileBP<AStaticMeshActor>(World, TileBlockBP, GenSpawnLoc, rotation);

				grid[row][col] = BlockTile;
				boolWallGrid[row][col] = true;
			}
		}			
	}

	//Now that the initial walls and floors are setup, we will need to make the edges of the graph
	//the edges will be between 2 consecutive floors (which will be at a distance of +2 from the left or from the top to the right or bottom)
	//construct an array of such edges
	int dx[2] = { 2, 0 };
	int dy[2] = { 0, 2 };
	TArray<FEdgeKruskal> edgeArray;
	for (int row = 1; row < Rows - 1; row += 2)
	{
		for (int col = 1; col < Columns - 1; col += 2)
		{
			//inner for loop to assign the edges
			for (int k = 0; k < 2; k++)
			{
				int toRow = row + dx[k];
				int toCol = col + dy[k];

				//bounds check
				if (toRow >= 1 && toRow < Rows - 1 && toCol >= 1 && toCol < Columns - 1)
				{
					edgeArray.Emplace(FEdgeKruskal(FCoordinate(row, col), FCoordinate(toRow, toCol)));
				}
			}
		}
	}

	//also make sure the maze borders have edges
	int N = Rows / 2;
	if (Rows % 2 == 0)
	{
		N--; //to get the potential number of vertices
		
		//make sure the borders are handled
		grid[Rows - 2][Columns - 2]->Destroy();
		grid[Rows - 2][Columns - 2] = SpawnTileBP<AStaticMeshActor>(World, TileGroundBP, FVector(StartX + (Columns - 2)*offset, StartY + (Rows - 2) * offset, StartZ), rotation);
		grid[Rows - 2][Columns - 3]->Destroy();
		grid[Rows - 2][Columns - 3] = SpawnTileBP<AStaticMeshActor>(World, TileGroundBP, FVector(StartX + (Columns - 3)*offset, StartY + (Rows - 2) * offset, StartZ), rotation);
	}
	if (Columns % 2 == 0)
	{
		N *= ((Columns / 2) - 1);

		grid[Rows - 2][Columns - 2]->Destroy();
		grid[Rows - 2][Columns - 2] = SpawnTileBP<AStaticMeshActor>(World, TileGroundBP, FVector(StartX + (Columns - 2)*offset, StartY + (Rows - 2) * offset, StartZ), rotation);
		grid[Rows - 3][Columns - 2]->Destroy();
		grid[Rows - 3][Columns - 2] = SpawnTileBP<AStaticMeshActor>(World, TileGroundBP, FVector(StartX + (Columns - 2)*offset, StartY + (Rows - 3) * offset, StartZ), rotation);
	}
	else
	{
		N *= (Columns / 2);
	}

	//once these edges are created, we have to sort them
	edgeArray.Sort([this](const FEdgeKruskal& lhs, const FEdgeKruskal& rhs) {
		return (FMath::FRand() < 0.5F);
	});

	//once the edges are sorted, we need to find check which subsets the from and to edges are in. If they are in the same subset, then they form a cycle and we discard them
	//otherwise we will union the from and to nodes to form the final edge in our spanning tree which will make the maze
	//we will do this N - 1 times because N - 1 will be the total edges in the min spanning tree generated
	int count = 0; //to keep the count of the number of edges processed
	int index = 0; //loop index
	while (count < N - 1 && edgeArray.Num() != 0)
	{
		FCoordinate from = edgeArray[index].From;
		FCoordinate to = edgeArray[index].To;

		//now find if these 2 belong to the same subset
		FCoordinate parentFrom = Find(from);
		FCoordinate parentTo = Find(to);

		if (parentFrom != parentTo) //they belong to disjoint subsets, means these 2 can form an edge, do a union
		{
			Union(parentFrom, parentTo);

			//now we need to spawn this edge in the map using a groundBP
			//for that we will need the row and col indices 
			int posRow = from.row;
			int posCol = from.col;

			if (posRow + 2 == to.row) //this would mean our offset is posRow+1
			{
				posRow++;
			}
			else
			{
				posCol++;
			}

			//if this position was occupied by a wall, we need to destroy it and then spawn a ground tile
			if (boolWallGrid[posRow][posCol])
			{				
				FVector FSpawnLoc = grid[posRow][posCol]->GetActorLocation();		
				FSpawnLoc.Y -= BlockPadding;

				grid[posRow][posCol]->Destroy();

				AStaticMeshActor* GroundTile = SpawnTileBP<AStaticMeshActor>(World, TileGroundBP, FSpawnLoc, rotation);
				grid[posRow][posCol] = GroundTile;
				boolWallGrid[posRow][posCol] = false;
			}

			count++;//update the count of the edges
		}
		index++;
	}

	//Finally spawn the start and the end tiles
	{
		FVector GenSpawnLoc = grid[1][1]->GetActorLocation();
		grid[1][1]->Destroy();
		//const FVector  GenSpawnLoc( StartX + 2 * offset, StartY + 2 * offset, StartZ);
		//Tile Start
		AStaticMeshActor* StartTile = SpawnTileBP<AStaticMeshActor>(World, TileStartBP, GenSpawnLoc, rotation);
		grid[1][1] = StartTile;
		boolWallGrid[1][1] = false;
	}

	{
		FVector GenSpawnLoc = grid[Rows - 2][Columns - 2]->GetActorLocation();
		grid[Rows - 2][Columns - 2]->Destroy();
		//FVector  GenSpawnLoc(StartX + ((Columns - 2) * offset), (StartY + (Rows - 2) * offset), StartZ);
		// Tile End
		AStaticMeshActor* EndTile = SpawnTileBP<AStaticMeshActor>(World, TileEndBP, GenSpawnLoc, rotation);
		EndTile->SetActorLabel("EndTile");
		grid[Rows - 2][Columns - 2] = EndTile;
		boolWallGrid[Rows - 2][Columns - 2] = false;
	}
}