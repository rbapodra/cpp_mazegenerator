// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DoorActor.generated.h"

/**
 * 
 */
UCLASS()
class CPP_MAZEGENERATOR_API ADoorActor : public AActor
{
	GENERATED_BODY()
	
public:
	ADoorActor(); /* Add Default values */
	
	/* Returns whether or not the door is Open*/
	UFUNCTION(BlueprintPure, Category = "DoorState")
		bool IsOpen();

	/* Allows other classes to safely change the state of the Door*/
	UFUNCTION(BlueprintCallable, Category = "DoorState")
		void SetState(bool bNewState);

	/*Every door has a particular key with which it can be opened */
	UPROPERTY(EditInstanceOnly)
		FString OpensWithKeyName;

protected:
	/* State of the door: Open or Closed. True means Open */
	bool bDoorState;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:

	/* Mesh representing the Door */
	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* DoorMesh;

	/*Box Component to be used as a collider */
	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* CollisionBox;
	
	/* Called when something enters the box component */
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/* Called when something leaves the box component */
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void Debug(FString msg);
	
	/* Rotation related attributes */
	float RotatedValue;
	FRotator CurrentDoorRotation;
};
