// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"

#include "SimpleBoxTrigger.generated.h"

/*!
**\ This will be a simple class to implement a Box Trigger event using a box component. We could have used the ATriggerBox class as well or used that as the base class
*/

UCLASS()
class CPP_MAZEGENERATOR_API ASimpleBoxTrigger : public AActor
{
	GENERATED_BODY()

	UBoxComponent* Box;

	UFUNCTION()
		void Debug(FString msg);

	UFUNCTION()
		void TriggerEnter(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void TriggerExit(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
public:	
	// Sets default values for this actor's properties
	ASimpleBoxTrigger();
};
