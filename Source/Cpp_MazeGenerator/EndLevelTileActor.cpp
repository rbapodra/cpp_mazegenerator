// Fill out your copyright notice in the Description page of Project Settings.

#include "EndLevelTileActor.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "MazeHUD.h"
#include "AMazeGameMode.h"

/*
**\Set all Defaults
*/
AEndLevelTileActor::AEndLevelTileActor()
{
	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxTrigger"));
	CollisionBox->bGenerateOverlapEvents = true;
	CollisionBox->Mobility = EComponentMobility::Static; //cannot move
	CollisionBox->InitBoxExtent(FVector(64.0f, 64.0f, 1.0f)); //these values can be edited from the Editor. We use a z-value of 1 because we don't really need a height component for the tile
	
	RootComponent = CollisionBox;

	//Bind the Overlap events
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AEndLevelTileActor::OnOverlapBegin);
	CollisionBox->OnComponentEndOverlap.AddDynamic(this, &AEndLevelTileActor::OnOverlapEnd);
}


void AEndLevelTileActor::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	FString ActorName = OtherActor->GetActorLabel();
	if (ActorName == "BP_ThirdPersonCharacter")
	{
		Debug(FString("Game Over! Level Completed!"));

		//TODO:: need to move this code in an object specific trigger
		//Change to the next level
		//NOTE:: we will have to keep track of the current level we are in, otherwise there is a possibility this might go in an infinite loop
		//Ideally, do this in Level Blueprint
		//For now we will keep track of it in the GameMode class
		auto world = GetWorld();
		FString CurrentLevelName = UGameplayStatics::GetCurrentLevelName(world);
		Debug("Current LevelName : " + CurrentLevelName);

		AAMazeGameMode* pMyGameMode = Cast<AAMazeGameMode>(GetWorld()->GetAuthGameMode());
		if (!pMyGameMode)
		{
			Debug(FString("Invalid GameMode!"));
			return;
		}
		FName newLevelName = pMyGameMode->GetNextLevelName(FName(*CurrentLevelName));
		if (CurrentLevelName != newLevelName.ToString() && newLevelName != "None")
		{
			UGameplayStatics::OpenLevel(world, newLevelName);
		}
		else
		{
			Debug(FString("GAME OVER!! All Levels Completed!"));
			//Get the HUD and turn on the widget
			AMazeHUD* pMazeHUD = Cast<AMazeHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
			if (pMazeHUD)
			{
				pMazeHUD->ShowGameOverWidget();
			}
		}

	}
	else
	{
		Debug(FString("Trigger Enter!"));
	}
}

void AEndLevelTileActor::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Debug(FString("Trigger Exit!"));
}

void AEndLevelTileActor::Debug(FString msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, msg);
	}
}