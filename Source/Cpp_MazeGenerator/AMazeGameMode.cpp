// Fill out your copyright notice in the Description page of Project Settings.

#include "AMazeGameMode.h"
#include "AThirdPersonCharacter.h"
#include "MazePlayerController.h"
#include "MazeHUD.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/PlayerStart.h"
#include "Engine/StaticMeshActor.h"
#include "UObject/ObjectRedirector.h"
#include "EngineUtils.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Kismet/GameplayStatics.h"

AAMazeGameMode::AAMazeGameMode()
{
	//load our third person character class as the default pawn class
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}	

	//set our custom player controller class
	PlayerControllerClass = AMazePlayerController::StaticClass();

	//set our custom HUD class 
	HUDClass = AMazeHUD::StaticClass();

	//Start the players as spectators initially
	//Note::uncomment only if the full level is procedural
	//bStartPlayersAsSpectators = true;

	//Create an array of all the level names here
	FArrayLevelNames.Add("MazeLevel1");
	FArrayLevelNames.Add("MazeLevel2");
	FArrayLevelNames.Add("MazeLevel3");

}

void AAMazeGameMode::ResetPlayerSpectatorMode()
{
	
}

/*
**\Gets the next level name in the array after the current level
**\If this is the last level, returns an empty string
**\TODO:: implement this using a queue
*/
FName AAMazeGameMode::GetNextLevelName(const FName& FCurrentLevelName)
{
	//First check if this is the last level
	if (FCurrentLevelName == FArrayLevelNames.Last())
	{
		return FName();
	}	

	//get the index of this level, increment it and return the level name corresponding to the new index
	int32 currentIndex = -1;
	if (FArrayLevelNames.Find(FCurrentLevelName, currentIndex))
	{
		return FArrayLevelNames[currentIndex + 1];
	}
	
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, "Level NOT FOUND!!");
	}
	return FName();
}

UClass* AAMazeGameMode::FindClasses(const FString& ClassName) const
{
	check(*ClassName);

	UObject* ClassPackage = ANY_PACKAGE;

	if (UClass* Result = FindObject<UClass>(ClassPackage, *ClassName))
		return Result;

	if (UObjectRedirector* RenamedClassRedirector = FindObject<UObjectRedirector>(ClassPackage, *ClassName))
		return CastChecked<UClass>(RenamedClassRedirector->DestinationObject);

	return nullptr;
}
/*!
**\Overriding the ChoosePlayerStart function to handle the spawning of the Player Pawn ourselves
*/
AActor* AAMazeGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	AActor* FoundPlayer = Super::ChoosePlayerStart_Implementation(Player);
	//Find the StartTile object and get its location. Once we have that location, use the setActorLocation function to set the final location.

	//loop through all the actors that belong to the BP_StartTile class. We will only have 2 of them with ID with no suffix and a one with suffix 1
	//We are looking for the one with no suffix.
	
	/*for (TObjectIterator<AStaticMeshActor> Itr; Itr; ++Itr)
	{
	FString fsActorName = Itr->GetName();
	}
	*/
	return FoundPlayer;
}
