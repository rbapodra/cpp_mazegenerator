// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Engine/StaticMeshActor.h>
#include "MazeGeneratorActor.generated.h"

struct FIntGrid
{
	TArray<int> Columns;

	FIntGrid()
	{
	}
};

struct FCoordinate
{
	int row;
	int col;

	FCoordinate()
	{
	}
	FCoordinate(int Row, int Col)
		:row(Row),
		 col(Col)
	{
	}
	FCoordinate(const FCoordinate& rhs)
		:row(rhs.row),
		 col(rhs.col)
	{
	}

	bool operator==(const FCoordinate& rhs) const
	{
		return (row == rhs.row && col == rhs.col);
	}

	bool operator!=(const FCoordinate& rhs) const
	{
		return !(*this == rhs);
	}

	FCoordinate& operator=(const FCoordinate& rhs)
	{

		row = rhs.row;
		col = rhs.col;
		return *this;
		
	}
};

//NOTE:: there is a struct called FEdge by Unreal that we can use
struct FEdgeKruskal
{
	//From, To edges
	FCoordinate From;
	FCoordinate To;
	//int nWeight;

	FEdgeKruskal()
	{
	}

	FEdgeKruskal(const FCoordinate& from, const FCoordinate& to)
		:From(from), 
		 To(to)
	{
	}
	
	bool operator==(const FEdgeKruskal &other) const {
		return From == other.From && To == other.To;
	}
};


UCLASS()
class CPP_MAZEGENERATOR_API AMazeGeneratorActor : public AActor
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable, Category = "MazeGen")
		void GenerateMaze();

	UFUNCTION(BlueprintCallable, Category = "MazeGen")
		void OddAlgo();

	UFUNCTION(BlueprintCallable, Category = "MazeGen")
		void DFS();

	UFUNCTION(BlueprintCallable, Category = "MazeGen")
		void Kruskal();

	UFUNCTION(BlueprintCallable, Category = "MazeGen")
		void SpawnMaze();

	void DFSRecursive(int row, int col);

	void SpawnEndLevelTrigger();
	/*!
	**\Simple helper function to list all the actor names
	*/
	void ListAllCurrentActors(); 
public:	
	// Sets default values for this actor's properties
	AMazeGeneratorActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MAZEHUD)
		bool ShowTests;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MazeProperties)
		int StartX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MazeProperties)
		int StartY;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MazeProperties)
		int StartZ;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MazeProperties)
		int Rows;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MazeProperties)
		int Columns;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = MazeTiles)
		UClass* TileGroundBP;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = MazeTiles)
		UClass* TileBlockBP;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = MazeTiles)
		UClass* TileStartBP;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = MazeTiles)
		UClass* TileEndBP;
	
	TArray<FIntGrid> MazeGrid;

	static const int MazeSizeMax = 101;
	static const float offset; //offset for placing the tiles
	static const float BlockPadding;
	static const FRotator rotation;

	//Note: for optimized version, use InstancedStaticMeshes
	AStaticMeshActor* grid[MazeSizeMax][MazeSizeMax];
	bool boolWallGrid[MazeSizeMax][MazeSizeMax]; //boolean matrix to determine if a particular cell is occupied by a wall or ground tile

	//For Kruskal's Algo only
	FCoordinate Parent[MazeSizeMax][MazeSizeMax]; //stores the root or the parent of a particular vertex
	int rank[MazeSizeMax][MazeSizeMax]; //stores the size of the particular root tree or a unique number to identify the particular subset

	//Union-Find methods
	FCoordinate Find(const FCoordinate& node); //finds the root / parent of a particular node in the tree
	void Union(const FCoordinate& node1, const FCoordinate& node2); //makes an edge out of the 2 nodes

	//Note:: the below code will kind of lock the tiles to be a child of AStaticMeshActor class. If we want to make it dynamic, we use UCLASS and customize it from the editor
/*	UPROPERTY(EditAnywhere)
		TSubclassOf<AStaticMeshActor> TileGroundBP;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AStaticMeshActor> TileBlockBP;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AStaticMeshActor> TilePillarBP;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AStaticMeshActor> TileStartBP;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AStaticMeshActor> TileEndBP;
*/
};

//NOTE:: code to spawn a blueprint from C++
template <typename AMazeGen>
FORCEINLINE AMazeGen* SpawnTileBP(
	UWorld* TheWorld,
	UClass* TheBP,
	const FVector& Loc,
	const FRotator& Rot,
	const bool bNoCollisionFail = true,
	AActor* Owner = NULL,
	APawn* Instigator = NULL
)
{
	if (!TheWorld)
	{
		return NULL;
	}

	if (!TheBP)
	{
		return NULL;
	}

	FActorSpawnParameters SpawnInfo;
	//because the newer UE4 version does not have this boolean, we will manually handle the bNoCollisionFail condition.
	//if true, then always Spawn, don't care about collision, otherwise try to adjust the position
	SpawnInfo.SpawnCollisionHandlingOverride = bNoCollisionFail ? (ESpawnActorCollisionHandlingMethod::AlwaysSpawn) : (ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);
	SpawnInfo.Owner = Owner;
	SpawnInfo.Instigator = Instigator;
	SpawnInfo.bDeferConstruction = false;

	return TheWorld->SpawnActor<AMazeGen>(TheBP, Loc, Rot, SpawnInfo);
}