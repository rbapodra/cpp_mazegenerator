// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MazeHUD.generated.h"

/**
 * 
 */
UCLASS()
class CPP_MAZEGENERATOR_API AMazeHUD : public AHUD
{
	GENERATED_BODY()
	

	//variable holding the Widget object after its creation
	UUserWidget* m_pGameOverWidget;

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widgets")
	TSubclassOf<class UUserWidget> wGameOver; //We will be using the BP_GameOverWidget asset

	virtual void BeginPlay() override;

	AMazeHUD();
	
	void ShowGameOverWidget();
};
