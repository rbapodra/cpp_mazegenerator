// Fill out your copyright notice in the Description page of Project Settings.

#include "PickupActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Engine/Engine.h"
#include "AThirdPersonCharacter.h"

// Sets default values
APickupActor::APickupActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//initially all pick up objects are true 
	bIsActive = true;

	//Create the static mesh component
	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->bGenerateOverlapEvents = true;
	//Bind the Overlap events
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &APickupActor::OnOverlapBegin);	
	CollisionBox->AttachToComponent(this->RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	RootComponent = PickupMesh;
}

// Called when the game starts or when spawned
void APickupActor::BeginPlay()
{
	Super::BeginPlay();
	
}

//returns active state
bool APickupActor::IsActive()
{
	return bIsActive;
}

/* Sets active state */
void APickupActor::SetActive(bool bNewState)
{
	bIsActive = bNewState;
}

/* Handle the Picking up logic here if the object is active */
void APickupActor::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		FString ActorName = OtherActor->GetActorLabel();
		if (ActorName == "BP_ThirdPersonCharacter")
		{
			//Add the pickup actor to the Character's owned list
			//NOTE: this could be a game-specific function
			AAThirdPersonCharacter* ThirdPersonCharacter = Cast<AAThirdPersonCharacter>(OtherActor);
			if (ThirdPersonCharacter)
			{
				ThirdPersonCharacter->AddKey(this->GetActorLabel());
			}

			FString msg = this->GetActorLabel();
			msg += " Picked!";
			Debug(msg);

			//Destroy this actor
			this->Destroy();
		}
	}
	
}

/* Prints Debug message*/
void APickupActor::Debug(FString msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, msg);
	}
}