// Fill out your copyright notice in the Description page of Project Settings.

#include "MazeHUD.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"
AMazeHUD::AMazeHUD()
{
	//we need to initialize the BP asset
	static ConstructorHelpers::FClassFinder<UUserWidget> widgetObject(TEXT("/Game/Blueprints/BP_GameOverWidget"));
	if (widgetObject.Succeeded())
	{
		wGameOver = widgetObject.Class;
	}
}

void AMazeHUD::ShowGameOverWidget()
{
	if (m_pGameOverWidget)
	{
		m_pGameOverWidget->SetVisibility(ESlateVisibility::Visible);
	}
}

void AMazeHUD::BeginPlay()
{
	Super::BeginPlay();

	//create the game over widget and hide it..
	if (wGameOver)
	{
		//create the widget and display it
		m_pGameOverWidget = CreateWidget<UUserWidget>(this->GetOwningPlayerController(), wGameOver);
		if (m_pGameOverWidget)
		{
			m_pGameOverWidget->AddToViewport();
			m_pGameOverWidget->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}
