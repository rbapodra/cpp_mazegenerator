// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorActor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "AThirdPersonCharacter.h"
#include "Engine/Engine.h"
#include "Kismet/KismetMathLibrary.h"

ADoorActor::ADoorActor()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxTrigger"));
	CollisionBox->bGenerateOverlapEvents = true;	
	CollisionBox->InitBoxExtent(FVector(50.0f, 50.0f, 50.0f)); 

	RootComponent = CollisionBox; //make the collision box as the root component

	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorObject"));
	DoorMesh->SetRelativeLocation(FVector(0.0f, 50.0f, -50.0f));
	//attach this to the root component
	DoorMesh->SetupAttachment(RootComponent);

	//Bind the Overlap events
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &ADoorActor::OnOverlapBegin);
	CollisionBox->OnComponentEndOverlap.AddDynamic(this, &ADoorActor::OnOverlapEnd);

	bDoorState = false; //initially door is closed
	OpensWithKeyName = ""; //Set this from the editor
}

bool ADoorActor::IsOpen()
{
	return bDoorState;
}

void ADoorActor::SetState(bool bNewState)
{
	bDoorState = bNewState;
}


void ADoorActor::BeginPlay()
{
	Super::BeginPlay();
}

/*
**\Handle the Door rotation interpolation in the Tick function based on the door state
*/
void ADoorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Get the current relative rotation
	CurrentDoorRotation = DoorMesh->RelativeRotation;

	//if the door is open, interpolate the rotation
	if (IsOpen())
	{
		DoorMesh->SetRelativeRotation(FMath::Lerp(FQuat(CurrentDoorRotation), FQuat(FRotator(0.0f, RotatedValue, 0.0f)), 0.05));  //alpha value = 0.01 
	}
	else
	{
		//Close the door,
		//Reset the RotatedValue = 0
		DoorMesh->SetRelativeRotation(FMath::Lerp(FQuat(CurrentDoorRotation), FQuat(FRotator(0.0f, 0.0f, 0.0f)), 0.05));  //0.01 is the alpha value for smooth interpolation which is kind of very slow, so we increased it
	}
	
}

/*
**\ Handle the object collision. 
**\ If the player enters the collision, check if he has the key to open the door
*/
void ADoorActor::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr)
	{
		FString ActorName = OtherActor->GetActorLabel();
		if (ActorName == "BP_ThirdPersonCharacter")
		{
			//Check if the player has the key of this door and also if the door is currently closed
			AAThirdPersonCharacter* ThirdPersonCharacter = Cast<AAThirdPersonCharacter>(OtherActor);
			if (ThirdPersonCharacter)
			{
				if (ThirdPersonCharacter->IsKeyOwned(OpensWithKeyName))
				{
					if (!bDoorState)
					{
						//Get the direction vector to find what direction the player is standing
						
						//Method 1:
						//FVector ActorLocation = OtherActor->GetActorLocation();
						//FVector DirectionVector = GetActorLocation() - ActorLocation;
						//DirectionVector = UKismetMathLibrary::LessLess_VectorRotator(DirectionVector, GetActorRotation());
						//RotatedValue = (DirectionVector.X > 0.0f) ? 90.0f : -90.0f;

						//Method 2: using simple Dot Product
						float DotProd = FVector::DotProduct(OtherActor->GetActorForwardVector(), GetActorForwardVector());

						if (FMath::Sign(DotProd) > 0)
						{
							RotatedValue = 90.0f;
						}
						else
						{
							RotatedValue = -90.0f;
						}						

						//Set the state of the door to open
						SetState(true);
						//TODO: later on we could play an animation
						FString msg("Door Open!");
						Debug(msg);						
					}
					
				}
				else
				{
					FString msg("Requires Key to open");
					Debug(msg);
				}
				
			}
		}
	}
}

/*
**\ If the player leaves the collision box, set the state of the door to closed.
*/
void ADoorActor::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		FString ActorName = OtherActor->GetActorLabel();
		if (ActorName == "BP_ThirdPersonCharacter")
		{
			//if the state was changed and to open, reset it to closed and make the mesh visible (OR play the close door animation)
			if (IsOpen())
			{
				SetState(false);
			}
		}
	}
}

/* Prints Debug message*/
void ADoorActor::Debug(FString msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, msg);
	}
}